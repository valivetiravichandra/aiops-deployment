from flask_restful import reqparse, Api, Resource
import numpy as np
from flask import Flask, request, redirect, url_for, flash, jsonify
import json, pickle
import re
import string
import json

my_model_list = [] 

clf=[]

keys=['site_name', 'category_name', 'subcategory_name', 'mode_name',
       'level_name', 'dept_name', 'impact_name', 'urgency_name',
       'priority_name', 'isescalated_x']

def load_model():
    with open('preprocess.pkl', 'rb') as f:
            clf.append(pickle.load(f))
    
    for x in keys:
        with open(x+".pkl", 'rb') as f:
            my_model_list.append(pickle.load(f)) 

            
app = Flask(__name__)
api = Api(app)

# argument parsing
parser = reqparse.RequestParser()
parser.add_argument('query')


class PredictAIOPS(Resource):
    def get(self):
        # use parser and find the user's query
        args = parser.parse_args()
        user_query = args['query']
        output=get_predictions(data_cleaning(user_query))
        # create JSON object
        #output = {'prediction': 'demo', 'confidence': 'demo'}        
        return json.dumps(output)
    
# Data Cleaning part
def data_cleaning(data_title):
    # Below regex gets word till whitespace is encountered
    ll=re.findall('\w+\S+', str(data_title))
    # Below code removes puntuations from the end of word like if string='North)' then output would be 'North'
    new_ll =[elem if elem[-1] not in string.punctuation else elem.strip(elem[-1]) for elem in ll]
    title_name=""
    title_name=" ".join(str(x) for x in new_ll)  
    return title_name

# Getting predictions from pickle
def get_predictions(title):
    title=np.array([data_cleaning(title)],dtype=object)
    transformed=clf[0].transform(title)
    pred_list=[]
    for x in my_model_list:
        pred_list.append(x.predict(transformed)[0])
    pred_dict = dict(zip(keys, pred_list))
    return pred_dict


api.add_resource(PredictAIOPS, '/')


if __name__ == '__main__':
    load_model() 
    app.run(debug=True)